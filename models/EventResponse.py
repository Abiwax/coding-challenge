from typing import List

from pydantic import BaseModel

from models.ScoreBoard import ScoreBoard
from models.Ranks import TeamRanking


class EventResponse:
    event_id: str
    event_date: str
    event_time: str
    away_team_id: str
    away_nick_name: str
    away_city: str
    away_rank: str
    away_rank_points: str
    home_team_id: str
    home_nick_name: str
    home_city: str
    home_rank: str
    home_rank_points: str

    def __init__(self, scoreboard: ScoreBoard, event_time: str, home_rank: TeamRanking, away_rank: TeamRanking) -> None:
        self.event_id = scoreboard.event_id
        self.event_date = scoreboard.event_date
        self.event_time = event_time
        self.away_team_id = scoreboard.away_team_id
        self.away_nick_name = scoreboard.away_nick_name
        self.away_city = scoreboard.away_city
        self.away_rank = away_rank.rank
        self.away_rank_points = away_rank.adjusted_points
        self.home_team_id = scoreboard.home_team_id
        self.home_nick_name = scoreboard.home_nick_name
        self.home_city = scoreboard.home_city
        self.home_rank = home_rank.rank
        self.home_rank_points = home_rank.adjusted_points


class EventResponseAPI(BaseModel):
    results: List[dict] = []

    class Config:
        schema_extra = {
            "example": {"results": [
                {'event_id': '1233827', 'event_date': '12-01-2020', 'event_time': '15:05', 'away_team_id': '42',
                 'away_nick_name': 'Texans', 'away_city': 'Houston', 'away_rank': '25',
                 'away_rank_points': '-6.41', 'home_team_id': '63', 'home_nick_name': 'Chiefs',
                 'home_city': 'Kansas City', 'home_rank': '5', 'home_rank_points': '11.38'}]}
        }

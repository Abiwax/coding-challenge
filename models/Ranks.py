class TeamRanking:
    team_id: str
    team: str
    rank: str
    last_week: str
    points: str
    modifier: str
    adjusted_points: str

    def __init__(self, json: dict) -> None:
        self.__dict__ = json

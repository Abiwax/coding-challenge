from pydantic import BaseModel


class EventParameter(BaseModel):
    start_date: str
    end_date: str
    league: str = "NFL"

    class Config:
        schema_extra = {
            "example": {"start_date": "string", "end_date": "string", "league": "NFL"}
        }

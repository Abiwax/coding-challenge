## Resulta Coding Challenge

Solution is a basic python script that can be deployed as an API using the FastAPI framework or executed locally using the terminal/commandline.

### How it works:

#### Assumptions
1. User is only allowed to enter dates with a difference of 7 days or less. If wrong dates are entered, no external API call is made. This is based on the API's requirement of date range must a maximum of 7 days.
2. User should be allowed to enter different leagues if needed. If no league is provided, the default NFL is used.
3. To avoid making unnecessary API calls, the scoreboard API should be called first to determine if data exists before calling the Team Ranking API.
4. Team Ranking result returned is an overall score and does not need to be checked by date.

#### Implementation
1. Start Date and End Date is requested from user, if user enters invalid dates based on the below criteria, an error is returned to the user.
   + String is not a valid date.
   + Days difference between dates is not greater than or equal to 0 and less or equal to 7.
2. League is requested from the user, since this is not in the requirements, a default value(NFL) is used if user does not provide a league.
3. If valid date is provided, an API call is made to the scoreboard API based on the start date, end date and league value.
4. If HTTP response status code is 200, the data is then formatted to allow easy retrieval of needed result in later processing.
5. If length of result returned from the scoreboard API is greater than 0, the team ranking API is then called.
6. If data returned by the Team Ranking API is also greater than 0 and HTTP Status code is 200, combination of both results is then carried out.

### Flow Chart Diagram of Solution
![Resulta Solution Flow Chart](images/solution.jpg)

### Folder structure
+ main.py - Main file that serves as an entry point for both the rest api service and the local execution of code.
+ EventParser.py - File containing declaration of an EventParser class that runs all the functionality required to complete the challenge.
+ requirements.txt - File containing declaration of all the packages needed to run the solution.
+ models
    + EventParameter.py - File containing the declaration of class EventParameter for body parameter of the API path.
    + EventResponse.py - File containing the declaration of class EventResponse for the final response returned to the user as requested in the challenge.
    + ScoreBoard.py - File containing the declaration of class ScoreBoard for the response returned from the scoreboard API.
    + Ranks.py - File containing the declaration of class ScoreBoard for the response returned from the team ranking API.
+ tests
    + EventParserTest.py - File containing unit tests for the EventParser class and its methods.

### Requirement
Code was written using Python 3.9 and tested on a linux based terminal.

1. To run this application install the requirements in the requirement.txt file.
   `pip install -r requirements.txt`
   
2. Set an environment variable containing your API key using the `API_KEY` variable. 
   `export API_KEY="your key"`

### Running
Code can be run using two methods:

### Local
Using a linux based terminal/command line

#### To run

```bash
    python3 main.py
```

### Via Swagger UI
API Based application built using FastAPI.
#### To run
```bash
    uvicorn main:app --reload
```
After running, Swagger UI can be accessed via http://127.0.0.1:8000/docs.

To use other API Development tools, API path is http://127.0.0.1:8000/event_parser

Path is a HTTP post method and expected request body is `start_date`, `end_date` and an optional `league` parameter.

##### Swagger UI
![Swagger UI](images/swagger.png)

requests==2.25.1
fastapi==0.63
uvicorn==0.13.3
python-dateutil==2.8.1
pydantic~=1.7.3

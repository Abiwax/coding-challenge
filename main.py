#!/usr/bin/env python3

# Main file for running code  either via input or API request.

from EventParser import EventParser
from fastapi import FastAPI, HTTPException
from models.EventResponse import EventResponseAPI
from models.EventParameter import EventParameter
import os

app = FastAPI(title="Resulta Coding Challenge",
              description="Basic API for getting the list of scores during the NFL season and thee individual team "
                          "rankings.",
              version="1.0.0")


@app.get("/", tags=["scoreboard_ranks"])
def home():
    return {"results": "Welcome to Resulta Challenge, use the event_parser url to process challenge."}


@app.post("/event_parser", response_model=EventResponseAPI, tags=["scoreboard_ranks"])
def get_score_ranks(event_parameter: EventParameter):
    api_key = os.getenv('API_KEY', '')
    if not api_key:
        raise HTTPException(status_code=400, detail={
            "error": "API_KEY not set in environment variable."})

    event_parser = EventParser(api_key)
    start_date, end_date, valid = event_parser.check_dates(event_parameter.start_date, event_parameter.end_date)

    if not valid:
        raise HTTPException(status_code=400, detail={
            "error": "Wrong date format passed. Ensure difference between dates is a maximum of 7 days."})

    response = event_parser.get_final_result(start_date, end_date)
    return {'results': response}


'''
    Retrieve user start date, end date and league using Standard input, 
    use input to get desired results from the EventParser class.
'''
if __name__ == '__main__':
    api_key = os.getenv('API_KEY', '')
    if not api_key:
        print("Can't find your API_KEY in the environment variable.")
    else:
        print('Enter Start Date:')

        start_date = str(input())

        print('Enter End Date:')

        end_date = str(input())

        print('Enter League if known, else NFL will be used:')
        league = str(input())

        event_parser = EventParser(api_key, league if league else "NFL")
        start_date, end_date, valid = event_parser.check_dates(start_date, end_date)

        if valid:
            print('Final Event Result:')
            print(event_parser.get_final_result(start_date, end_date))
        else:
            print(
                'Failed to generate result based on date passed, please check your date and try again.\nEnsure '
                'difference between dates is a maximum of 7 days.')

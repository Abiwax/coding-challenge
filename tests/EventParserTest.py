#!/usr/bin/env python3

import unittest
from unittest.mock import Mock, patch

from EventParser import EventParser


def requests_get_mock(param=None):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if param:
        return MockResponse(param, 200)

    return MockResponse(None, 404)


class EventParserTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.event_parser = EventParser()
        self.mock_rank_data = {'results': {'data': [
            {'team_id': '53', 'team': 'Buffalo', 'rank': '1', 'adjusted_points': '49.691'},
            {'team_id': '70', 'team': 'New Orleans', 'rank': '2', 'adjusted_points': '47.066'},
            {'team_id': '68', 'team': 'Tampa Bay', 'rank': '3', 'adjusted_points': '25.747'}]}}
        self.mock_rank_data_match = {"results": {"data": [{"team_id": "63", "rank": "5", "adjusted_points": "11.375"},
                                                          {"team_id": "62", "rank": "18", "adjusted_points": "-0.970"},
                                                          {"team_id": "42", "rank": "25",
                                                           "adjusted_points": "-6.410"}]}}
        self.first_event = {
            "1233827": {"event_id": "1233827", "event_date": "2020-01-12 15:05", "away_team_id": "42",
                        "away_name": "Houston Texans", "tv_station": "CBS", "tv_station_name": "CBS",
                        "away_nick_name": "Texans", "away_city": "Houston",
                        "home_team_id": "63", "home_name": "Kansas City Chiefs", "home_nick_name": "Chiefs",
                        "home_city": "Kansas City"}}
        self.second_event = {
            "1234560": {"event_id": "1234560", "event_date": "2020-01-19 15:05", "tv_station": "CBS",
                        "tv_station_name": "CBS", "away_team_id": "62", "away_name": "Tennessee Titans",
                        "away_nick_name": "Titans", "away_city": "Tennessee",
                        "home_team_id": "63", "home_name": "Kansas City Chiefs", "home_nick_name": "Chiefs",
                        "home_city": "Kansas City"}}
        self.mock_score_data = {
            "results": {"2020-01-12": {"data": self.first_event}, "2020-01-13": [], "2020-01-14": [],
                        "2020-01-19": {"data": self.second_event}}}

        self.formatted_score = [self.first_event, self.second_event]
        self.event_response = [
            {"event_id": "1233827", "event_date": "12-01-2020", "event_time": "15:05", "away_team_id": "42",
             "away_nick_name": "Texans", "away_city": "Houston", "away_rank": "25", "away_rank_points": "-6.41",
             "home_team_id": "63", "home_nick_name": "Chiefs", "home_city": "Kansas City", "home_rank": "5",
             "home_rank_points": "11.38"},
            {"event_id": "1234560", "event_date": "19-01-2020", "event_time": "15:05", "away_team_id": "62",
             "away_nick_name": "Titans", "away_city": "Tennessee", "away_rank": "18", "away_rank_points": "-0.97",
             "home_team_id": "63", "home_nick_name": "Chiefs", "home_city": "Kansas City", "home_rank": "5",
             "home_rank_points": "11.38"}]

    def test_init(self):
        self.assertIsInstance(self.event_parser.ranks, dict)
        self.assertIsInstance(self.event_parser.scores, dict)

    def test_format_rank_result(self):
        ranks = self.event_parser.format_rank_result(self.mock_rank_data)
        self.assertListEqual(list(ranks.keys()), ['53', '70', '68'])

    @patch('EventParser.requests.get')
    def test_make_api_call(self, requests_get):
        requests_get.return_value = Mock(ok=True, status_code=200)
        requests_get.return_value.json.return_value = self.mock_rank_data

        ranks = self.event_parser.make_api_call('testapi')

        self.assertDictEqual(ranks, self.mock_rank_data)

    @patch('EventParser.requests.get')
    def test_make_failed_api_call(self, requests_get):
        requests_get.return_value = Mock(ok=False, status_code=400)
        requests_get.return_value.json.return_value = self.mock_rank_data

        ranks = self.event_parser.make_api_call('testapi')

        self.assertDictEqual(ranks, {})

    @patch('EventParser.requests.get')
    def test_rank_retrieval(self, requests_get):
        requests_get.return_value = Mock(ok=True, status_code=200)
        requests_get.return_value.json.return_value = self.mock_rank_data

        self.event_parser.retrieve_ranks()

        self.assertListEqual(list(self.event_parser.ranks.keys()), ['53', '70', '68'])

    def test_format_scores_no_data(self):
        result = self.event_parser.format_scores({})
        self.assertEqual(len(result), 0)

    def test_format_scores(self):
        result = self.event_parser.format_scores(self.mock_score_data)
        self.assertEqual(len(result), 2)
        self.assertListEqual(result, [self.first_event, self.second_event])

    @patch('EventParser.requests.get')
    def test_retrieve_scoreboard(self, requests_get):
        requests_get.return_value = Mock(ok=True, status_code=200)
        requests_get.return_value.json.return_value = self.mock_score_data

        self.event_parser.retrieve_scoreboard('', '')
        self.assertEqual(len(self.event_parser.scores), 2)

    def test_combine_no_score_ranks(self):
        self.event_parser.scores = {}
        self.event_parser.ranks = self.event_parser.format_rank_result(self.mock_rank_data_match)
        final_result = self.event_parser.get_final_result('', '')
        self.assertListEqual(final_result, [])

    def test_check_wrong_dates(self):
        _, _, valid = self.event_parser.check_dates('', '')
        self.assertFalse(valid)

    def test_check_dates(self):
        start, end, valid = self.event_parser.check_dates('2020/12/02', '2020/12/07')
        self.assertTrue(valid)
        self.assertEqual('2020-12-02', start)
        self.assertEqual('2020-12-07', end)

    def test_combine_no_result_value_ranks(self):
        self.event_parser.scores = {'results': {}}
        self.event_parser.ranks = self.event_parser.format_rank_result(self.mock_rank_data_match)
        final_result = self.event_parser.get_final_result('', '')
        self.assertListEqual(final_result, [])

    def test_combine_score_ranks(self):
        self.event_parser.scores = self.formatted_score
        self.event_parser.ranks = self.event_parser.format_rank_result(self.mock_rank_data_match)
        final_result = self.event_parser.combine_score_ranks()

        self.assertListEqual(final_result, self.event_response)

    @patch('EventParser.requests.get')
    def test_get_final_result_no_value(self, requests_get):
        requests_get.side_effect = [requests_get_mock(),
                                    requests_get_mock()]
        self.assertListEqual(self.event_parser.get_final_result('', ''), [])

    @patch('EventParser.requests.get')
    def test_get_final_result_no_rank_value(self, requests_get):
        requests_get.side_effect = [requests_get_mock(),
                                    requests_get_mock(self.mock_score_data)]
        self.assertListEqual(self.event_parser.get_final_result('', ''), [])

    @patch('EventParser.requests.get')
    def test_get_final_result_no_score_value(self, requests_get):
        requests_get.side_effect = [requests_get_mock(self.mock_rank_data),
                                    requests_get_mock()]

        self.assertListEqual(self.event_parser.get_final_result('', ''), [])

    @patch('EventParser.requests.get')
    def test_get_final_result_no_rank_match(self, requests_get):
        requests_get.side_effect = [requests_get_mock(self.mock_rank_data),
                                    requests_get_mock(self.mock_score_data)]

        self.assertListEqual(self.event_parser.get_final_result('', ''), [])

    @patch('EventParser.requests.get')
    def test_get_final_result(self, requests_get):
        requests_get.side_effect = [requests_get_mock(self.mock_score_data),
                                    requests_get_mock(self.mock_rank_data_match)]

        final_result = self.event_parser.get_final_result('', '')
        self.assertEqual(len(final_result), 2)
        self.assertListEqual(final_result, self.event_response)


if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3

from functools import reduce

import requests
from models.EventResponse import EventResponse
from dateutil import parser

from models.Ranks import TeamRanking
from models.ScoreBoard import ScoreBoard


class EventParser:

    def __init__(self, api_key, league="NFL"):
        self.__api_key = api_key
        self.__main_url = "https://delivery.chalk247.com"
        self.__scoreboard_url = "scoreboard"
        self.__rank_url = "team_rankings"
        self.__league = league

        self.ranks = dict()
        self.scores = dict()

    '''
        result: API returned response
        Format API response into a dictionary containing the team id as a key and team information as its value:
        e.g. {"82": {"rank": 2, ...}}
    '''
    def format_rank_result(self, result):
        rank_value = dict()
        if "results" in result:
            result_value = result["results"]
            if "data" in result_value:
                data_value = result_value["data"]
                rank_value = reduce(lambda r, team: r.update({team['team_id']: TeamRanking(team)}) or r, data_value, {})

        return rank_value

    '''
        url: URL for requests to be made
        General method to be used by other methods for making API requests
    '''
    def make_api_call(self, url):
        requests_call = requests.get(url, params={'api_key': self.__api_key})
        requests_json = {}
        if requests_call.status_code == 200:
            requests_json = requests_call.json()

        return requests_json

    '''
        Method for calling the team ranking API and formatting its results.
    '''
    def retrieve_ranks(self):
        ranks_json = self.make_api_call("{0}/{1}/{2}".format(self.__main_url, self.__rank_url, self.__league))

        self.ranks = self.format_rank_result(ranks_json)

    '''
        scores: API returned response from scoreboard
        Method for returning the dictionaries of each event in the scoreboard by event id.
    '''
    def format_scores(self, scores):
        combined_data = []
        if 'results' in scores:
            results_data = scores['results']
            for i in results_data.keys():
                if 'data' in results_data[i]:
                    event_data = results_data[i]['data']
                    combined_data.append(event_data)
        return combined_data

    '''
        Method for combining results from both the scoreboard API and the team ranking API into the required
        output format. Method ensures certain fields exists before proceeding to next execution.
        If certain fields don't exists, an empty result is returned.
    '''
    def combine_score_ranks(self):
        scores = self.scores
        combined_data = []
        for event_data in scores:
            for j in event_data.keys():
                event_scores = ScoreBoard(event_data[j])
                if event_scores.event_date:
                    date = event_scores.event_date
                    time = ''
                    try:
                        converted_date_time = parser.parse(date)
                        date = converted_date_time.date().strftime('%d-%m-%Y')
                        time = converted_date_time.time().strftime('%H:%M')
                    except Exception as e:
                        pass

                    event_scores.event_date = date
                    event_scores.event_time = time

                    if event_scores.away_team_id and event_scores.home_team_id:
                        away_team_id = event_scores.away_team_id
                        home_team_id = event_scores.home_team_id

                        if away_team_id in self.ranks and home_team_id in self.ranks:
                            away_team_value = self.ranks[away_team_id]
                            home_team_value = self.ranks[home_team_id]

                            away_team_points = round(float(away_team_value.adjusted_points), 2)
                            home_team_points = round(float(home_team_value.adjusted_points), 2)

                            away_team_value.adjusted_points = str(away_team_points)
                            home_team_value.adjusted_points = str(home_team_points)

                            event_response = EventResponse(event_scores, time, home_team_value, away_team_value)
                            combined_data.append(event_response.__dict__)
        return combined_data

    '''
        start_date: Start date for Event Scores
        end_date: End date for Event Scores
        
        Method for calling the scoreboard API and formatting its results.
    '''
    def retrieve_scoreboard(self, start_date, end_date):
        scoreboard_url = "{0}/{1}/{2}/{3}/{4}".format(self.__main_url, self.__scoreboard_url, self.__league, start_date, end_date)

        scores_data = self.make_api_call(scoreboard_url)
        self.scores = self.format_scores(scores_data)

    '''
        start_date: Start date for Event Scores
        end_date: End date for Event Scores

        Method for checking if date is valid based on criterias outlined in Readme
    '''
    def check_dates(self, start_date, end_date):
        valid = False
        try:
            converted_start_date = parser.parse(start_date)
            converted_end_date = parser.parse(end_date)
            start_date = converted_start_date.date().strftime('%Y-%m-%d')
            end_date = converted_end_date.date().strftime('%Y-%m-%d')
            date_diff = abs((converted_end_date - converted_start_date).days)
            valid = 0 <= date_diff <= 7
        except Exception as e:
            start_date = None
            end_date = None

        return start_date, end_date, valid

    '''
        start_date: Start date for Event Scores
        end_date: End date for Event Scores

        Method serving as an entrypoint for retrieving desired challenge output
    '''
    def get_final_result(self, start_date, end_date):
        final_result = []
        self.retrieve_scoreboard(start_date, end_date)

        if len(self.scores) > 0:
            self.retrieve_ranks()
            if len(self.ranks.keys()) > 0:
                final_result = self.combine_score_ranks()

        return final_result
